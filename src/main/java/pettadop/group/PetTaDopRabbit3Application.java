package pettadop.group;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@SpringBootApplication
public class PetTaDopRabbit3Application 
{
	
	private final static String QUEUE_NAME = "PetTaDopMQ";
	
	public static final String ACCOUNT_SID =
            "ACfa70b55ad0f6e2fd05d0d41f5f6c8931";
    public static final String AUTH_TOKEN =
            "2e1201887185d89077b3ab4b6659b7cb";
    
    static String SMSuserName = "";
    static String SMSpetName = "";
    static String SMScell = "";
    static String SMSstring;

	public static void main(String[] args) throws Exception
	{

		
		SpringApplication.run(PetTaDopRabbit3Application.class, args);
		
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();

	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
	    
	    DeliverCallback deliverCallback = (consumerTag, delivery) -> {
	        String message = new String(delivery.getBody(), "UTF-8");  
	        System.out.println(" [x] Received '" + message + "'");
	        
	        String[] values = message.split(",");
	        
		    SMSuserName = values[0];
		    SMSpetName = values[1];
		    
		    //format this number
		    SMScell = "+27"+values[2].substring(1,10);
		    System.out.println(SMScell);
		    
		    SMSstring = "Congratulations " + SMSuserName + ". You have requested adoption for " + SMSpetName + ". A representative will be calling you shortly.";
		    System.out.println(SMSstring);
		    
		    //This is for the Sms sending
		    
		    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		    

	        Message messageTwo = Message
	                .creator(new PhoneNumber(SMScell), // to
	                        new PhoneNumber("+17748477045"), // from
	                        SMSstring)
	                .create();

	        System.out.println(messageTwo.getSid());
		    
	    };
	    channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });

	    
	}

}
